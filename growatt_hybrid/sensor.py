import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import sensor, modbus
from esphome.const import (
    CONF_ACTIVE_POWER,
    CONF_CURRENT,
    CONF_FREQUENCY,
    CONF_ID,
    CONF_VOLTAGE,
    DEVICE_CLASS_CURRENT,
    DEVICE_CLASS_ENERGY,
    DEVICE_CLASS_POWER,
    DEVICE_CLASS_VOLTAGE,
    DEVICE_CLASS_BATTERY,
    ICON_CURRENT_AC,
    STATE_CLASS_MEASUREMENT,
    STATE_CLASS_TOTAL_INCREASING,
    UNIT_AMPERE,
    UNIT_CELSIUS,
    UNIT_HERTZ,
    UNIT_VOLT,
    UNIT_WATT,
    UNIT_PERCENT,
)

CONF_PHASE_A = "phase_a"
CONF_PHASE_B = "phase_b"
CONF_PHASE_C = "phase_c"
CONF_TOTAL = "total"

CONF_ENERGY_PRODUCTION_DAY = "energy_production_day"
CONF_TOTAL_ENERGY_PRODUCTION = "total_energy_production"
CONF_TOTAL_GENERATION_TIME = "total_generation_time"
CONF_TODAY_GENERATION_TIME = "today_generation_time"
CONF_PV1 = "pv1"
CONF_PV2 = "pv2"
UNIT_KILOWATT_HOURS = "kWh"
UNIT_HOURS = "h"
UNIT_KOHM = "kΩ"
UNIT_MILLIAMPERE = "mA"

CONF_INVERTER_STATUS = "inverter_status"
CONF_PV_ACTIVE_POWER = "pv_active_power"
CONF_INVERTER_MODULE_TEMP = "inverter_module_temp"
CONF_INVERTER_IPM_TEMP = "inverter_ipm_temp"
CONF_INVERTER_BOOST_TEMP = "inverter_boost_temp"
CONF_PROTOCOL_VERSION = "protocol_version"

CONF_BATTERY_SOC = "battery_soc"
CONF_BATTERY_VDC = "battery_vdc"
CONF_BATTERY_TEMPERATURE = "battery_temperature"
CONF_BATTERY_CHARGE_POWER = "battery_charge_power"
CONF_BATTERY_DISCHARGE_POWER = "battery_discharge_power"

CONF_REC_TEMPERATURE = "rec_temperature"

CONF_GRID_VAC_RS = "grid_vac_rs"
CONF_GRID_VAC_ST = "grid_vac_st"
CONF_GRID_VAC_TR = "grid_vac_tr"

CONF_EAC_TODAY = "eac_today"
CONF_EAC_TOTAL = "eac_total"

CONF_PV1_TODAY = "pv1_today"
CONF_PV1_TOTAL = "pv1_total"
CONF_PV2_TODAY = "pv2_today"
CONF_PV2_TOTAL = "pv2_total"

CONF_PAC_TO_USER = "pac_to_user"
CONF_PAC_TO_GRID = "pac_to_grid"
CONF_PAC_TO_LOAD = "pac_to_load"

CONF_TODAY = "today"
CONF_ENERGY_TOUSER = "energy_to_user"
CONF_ENERGY_TOGRID = "energy_to_grid"
CONF_ENERGY_TOLOAD = "energy_to_load"
CONF_ENERGY_CHARGE = "energy_charge"
CONF_ENERGY_DISCHARGE = "energy_discharge"

AUTO_LOAD = ["modbus"]
CODEOWNERS = ["@leeuwte"]

growatt_hybrid_ns = cg.esphome_ns.namespace("growatt_hybrid")
GrowattHybrid = growatt_hybrid_ns.class_(
    "GrowattHybrid", cg.PollingComponent, modbus.ModbusDevice
)

PHASE_SENSORS = {
    CONF_VOLTAGE: sensor.sensor_schema(
        unit_of_measurement=UNIT_VOLT,
        accuracy_decimals=2,
        device_class=DEVICE_CLASS_VOLTAGE,
    ),
    CONF_CURRENT: sensor.sensor_schema(
        unit_of_measurement=UNIT_AMPERE,
        accuracy_decimals=2,
        device_class=DEVICE_CLASS_CURRENT,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
    CONF_ACTIVE_POWER: sensor.sensor_schema(
        unit_of_measurement=UNIT_WATT,
        accuracy_decimals=0,
        device_class=DEVICE_CLASS_POWER,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
}
PV_SENSORS = {
    CONF_VOLTAGE: sensor.sensor_schema(
        unit_of_measurement=UNIT_VOLT,
        accuracy_decimals=2,
        device_class=DEVICE_CLASS_VOLTAGE,
    ),
    CONF_CURRENT: sensor.sensor_schema(
        unit_of_measurement=UNIT_AMPERE,
        accuracy_decimals=2,
        device_class=DEVICE_CLASS_CURRENT,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
    CONF_ACTIVE_POWER: sensor.sensor_schema(
        unit_of_measurement=UNIT_WATT,
        accuracy_decimals=0,
        device_class=DEVICE_CLASS_POWER,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
}
PHASE_POWER_SENSORS = {
    CONF_PHASE_A: sensor.sensor_schema(
        unit_of_measurement=UNIT_WATT,
        accuracy_decimals=0,
        device_class=DEVICE_CLASS_POWER,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
    CONF_PHASE_B: sensor.sensor_schema(
        unit_of_measurement=UNIT_WATT,
        accuracy_decimals=0,
        device_class=DEVICE_CLASS_POWER,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
    CONF_PHASE_C: sensor.sensor_schema(
        unit_of_measurement=UNIT_WATT,
        accuracy_decimals=0,
        device_class=DEVICE_CLASS_POWER,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
    CONF_TOTAL: sensor.sensor_schema(
        unit_of_measurement=UNIT_WATT,
        accuracy_decimals=0,
        device_class=DEVICE_CLASS_POWER,
        state_class=STATE_CLASS_MEASUREMENT,
    ),
}
ENERGY_SENSORS  = {
    CONF_TODAY: sensor.sensor_schema(
        unit_of_measurement=UNIT_KILOWATT_HOURS,
        accuracy_decimals=2,
        device_class=DEVICE_CLASS_ENERGY,
        state_class=STATE_CLASS_TOTAL_INCREASING,
    ),
    CONF_TOTAL: sensor.sensor_schema(
        unit_of_measurement=UNIT_KILOWATT_HOURS,
        accuracy_decimals=2,
        device_class=DEVICE_CLASS_ENERGY,
        state_class=STATE_CLASS_TOTAL_INCREASING,
    ),
}
PHASE_SCHEMA = cv.Schema(
    {cv.Optional(sensor): schema for sensor, schema in PHASE_SENSORS.items()}
)
PV_SCHEMA = cv.Schema(
    {cv.Optional(sensor): schema for sensor, schema in PV_SENSORS.items()}
)
PHASE_POWER_SCHEMA = cv.Schema(
    {cv.Optional(sensor): schema for sensor, schema in PHASE_POWER_SENSORS.items()}
)
ENERGY_SCHEMA = cv.Schema(
    {cv.Optional(sensor): schema for sensor, schema in ENERGY_SENSORS.items()}
)

GrowattProtocolVersion = growatt_hybrid_ns.enum("GrowattProtocolVersion")
PROTOCOL_VERSIONS = {
    "RTU": GrowattProtocolVersion.RTU,
    "RTU2": GrowattProtocolVersion.RTU2,
}


CONFIG_SCHEMA = (
    cv.Schema(
        {
            cv.GenerateID(): cv.declare_id(GrowattHybrid),
            cv.Optional(CONF_PROTOCOL_VERSION, default="RTU"): cv.enum(
                PROTOCOL_VERSIONS, upper=True
            ),
            cv.Optional(CONF_PHASE_A): PHASE_SCHEMA,
            cv.Optional(CONF_PHASE_B): PHASE_SCHEMA,
            cv.Optional(CONF_PHASE_C): PHASE_SCHEMA,
            cv.Optional(CONF_PV1): PV_SCHEMA,
            cv.Optional(CONF_PV2): PV_SCHEMA,
            cv.Optional(CONF_INVERTER_STATUS): sensor.sensor_schema(),
            cv.Optional(CONF_FREQUENCY): sensor.sensor_schema(
                unit_of_measurement=UNIT_HERTZ,
                icon=ICON_CURRENT_AC,
                accuracy_decimals=2,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_ACTIVE_POWER): sensor.sensor_schema(
                unit_of_measurement=UNIT_WATT,
                accuracy_decimals=0,
                device_class=DEVICE_CLASS_POWER,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_PV_ACTIVE_POWER): sensor.sensor_schema(
                unit_of_measurement=UNIT_WATT,
                accuracy_decimals=0,
                device_class=DEVICE_CLASS_POWER,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_ENERGY_PRODUCTION_DAY): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_TOTAL_ENERGY_PRODUCTION): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=0,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_INVERTER_MODULE_TEMP): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_INVERTER_IPM_TEMP): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_INVERTER_BOOST_TEMP): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_BATTERY_SOC): sensor.sensor_schema(
                unit_of_measurement=UNIT_PERCENT,
                accuracy_decimals=0,
                device_class=DEVICE_CLASS_BATTERY,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_BATTERY_VDC): sensor.sensor_schema(
                unit_of_measurement=UNIT_VOLT,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_VOLTAGE,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_GRID_VAC_RS): sensor.sensor_schema(
                unit_of_measurement=UNIT_VOLT,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_VOLTAGE,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_GRID_VAC_ST): sensor.sensor_schema(
                unit_of_measurement=UNIT_VOLT,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_VOLTAGE,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_GRID_VAC_TR): sensor.sensor_schema(
                unit_of_measurement=UNIT_VOLT,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_VOLTAGE,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_EAC_TODAY): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_EAC_TOTAL): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_PV1_TODAY): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_PV1_TOTAL): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_PV2_TODAY): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_PV2_TOTAL): sensor.sensor_schema(
                unit_of_measurement=UNIT_KILOWATT_HOURS,
                accuracy_decimals=2,
                device_class=DEVICE_CLASS_ENERGY,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
            cv.Optional(CONF_BATTERY_CHARGE_POWER): sensor.sensor_schema(
                unit_of_measurement=UNIT_WATT,
                accuracy_decimals=0,
                device_class=DEVICE_CLASS_POWER,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_BATTERY_DISCHARGE_POWER): sensor.sensor_schema(
                unit_of_measurement=UNIT_WATT,
                accuracy_decimals=0,
                device_class=DEVICE_CLASS_POWER,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_PAC_TO_USER): PHASE_POWER_SCHEMA,
            cv.Optional(CONF_PAC_TO_GRID): PHASE_POWER_SCHEMA,
            cv.Optional(CONF_PAC_TO_LOAD): PHASE_POWER_SCHEMA,
            cv.Optional(CONF_REC_TEMPERATURE): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_BATTERY_TEMPERATURE): sensor.sensor_schema(
                unit_of_measurement=UNIT_CELSIUS,
                accuracy_decimals=1,
                state_class=STATE_CLASS_MEASUREMENT,
            ),
            cv.Optional(CONF_ENERGY_TOUSER): ENERGY_SCHEMA,
            cv.Optional(CONF_ENERGY_TOLOAD): ENERGY_SCHEMA,
            cv.Optional(CONF_ENERGY_TOGRID): ENERGY_SCHEMA,
            cv.Optional(CONF_ENERGY_CHARGE): ENERGY_SCHEMA,
            cv.Optional(CONF_ENERGY_DISCHARGE): ENERGY_SCHEMA,
        }
    )
    .extend(cv.polling_component_schema("10s"))
    .extend(modbus.modbus_device_schema(0x01))
)


async def to_code(config):
    var = cg.new_Pvariable(config[CONF_ID])
    await cg.register_component(var, config)
    await modbus.register_modbus_device(var, config)

    cg.add(var.set_protocol_version(config[CONF_PROTOCOL_VERSION]))

    if CONF_INVERTER_STATUS in config:
        sens = await sensor.new_sensor(config[CONF_INVERTER_STATUS])
        cg.add(var.set_inverter_status_sensor(sens))

    if CONF_FREQUENCY in config:
        sens = await sensor.new_sensor(config[CONF_FREQUENCY])
        cg.add(var.set_grid_frequency_sensor(sens))

    if CONF_ACTIVE_POWER in config:
        sens = await sensor.new_sensor(config[CONF_ACTIVE_POWER])
        cg.add(var.set_grid_active_power_sensor(sens))

    if CONF_PV_ACTIVE_POWER in config:
        sens = await sensor.new_sensor(config[CONF_PV_ACTIVE_POWER])
        cg.add(var.set_pv_active_power_sensor(sens))

    if CONF_ENERGY_PRODUCTION_DAY in config:
        sens = await sensor.new_sensor(config[CONF_ENERGY_PRODUCTION_DAY])
        cg.add(var.set_today_production_sensor(sens))

    if CONF_TOTAL_ENERGY_PRODUCTION in config:
        sens = await sensor.new_sensor(config[CONF_TOTAL_ENERGY_PRODUCTION])
        cg.add(var.set_total_energy_production_sensor(sens))

    if CONF_INVERTER_MODULE_TEMP in config:
        sens = await sensor.new_sensor(config[CONF_INVERTER_MODULE_TEMP])
        cg.add(var.set_inverter_module_temp_sensor(sens))
    
    if CONF_INVERTER_IPM_TEMP in config:
        sens = await sensor.new_sensor(config[CONF_INVERTER_IPM_TEMP])
        cg.add(var.set_inverter_ipm_temp_sensor(sens))
    
    if CONF_INVERTER_BOOST_TEMP in config:
        sens = await sensor.new_sensor(config[CONF_INVERTER_BOOST_TEMP])
        cg.add(var.set_inverter_boost_temp_sensor(sens))

    for i, phase in enumerate([CONF_PHASE_A, CONF_PHASE_B, CONF_PHASE_C]):
        if phase not in config:
            continue

        phase_config = config[phase]
        for sensor_type in PHASE_SENSORS:
            if sensor_type in phase_config:
                sens = await sensor.new_sensor(phase_config[sensor_type])
                cg.add(getattr(var, f"set_{sensor_type}_sensor")(i, sens))

    for i, pv in enumerate([CONF_PV1, CONF_PV2]):
        if pv not in config:
            continue

        pv_config = config[pv]
        for sensor_type in pv_config:
            if sensor_type in pv_config:
                sens = await sensor.new_sensor(pv_config[sensor_type])
                cg.add(getattr(var, f"set_{sensor_type}_sensor_pv")(i, sens))
    
    if CONF_BATTERY_SOC in config:
        sens = await sensor.new_sensor(config[CONF_BATTERY_SOC])
        cg.add(var.set_battery_soc_sensor(sens))
    
    if CONF_BATTERY_VDC in config:
        sens = await sensor.new_sensor(config[CONF_BATTERY_VDC])
        cg.add(var.set_battery_vdc_sensor(sens))
        
    if CONF_BATTERY_CHARGE_POWER in config:
        sens = await sensor.new_sensor(config[CONF_BATTERY_CHARGE_POWER])
        cg.add(var.set_battery_charge_power_sensor(sens))
        
    if CONF_BATTERY_DISCHARGE_POWER in config:
        sens = await sensor.new_sensor(config[CONF_BATTERY_DISCHARGE_POWER])
        cg.add(var.set_battery_discharge_power_sensor(sens))

    if CONF_GRID_VAC_RS in config:
        sens = await sensor.new_sensor(config[CONF_GRID_VAC_RS])
        cg.add(var.set_grid_vac_rs_sensor(sens))
        
    if CONF_GRID_VAC_ST in config:
        sens = await sensor.new_sensor(config[CONF_GRID_VAC_ST])
        cg.add(var.set_grid_vac_st_sensor(sens))
        
    if CONF_GRID_VAC_TR in config:
        sens = await sensor.new_sensor(config[CONF_GRID_VAC_TR])
        cg.add(var.set_grid_vac_tr_sensor(sens))
           
    if CONF_EAC_TODAY in config:
        sens = await sensor.new_sensor(config[CONF_EAC_TODAY])
        cg.add(var.set_eac_today_sensor(sens))
        
    if CONF_EAC_TOTAL in config:
        sens = await sensor.new_sensor(config[CONF_EAC_TOTAL])
        cg.add(var.set_eac_total_sensor(sens))
        
    if CONF_PV1_TODAY in config:
        sens = await sensor.new_sensor(config[CONF_PV1_TODAY])
        cg.add(var.set_pv1_today_production_sensor(sens))
                
    if CONF_PV1_TOTAL in config:
        sens = await sensor.new_sensor(config[CONF_PV1_TOTAL])
        cg.add(var.set_pv1_total_production_sensor(sens))
        
    if CONF_PV2_TODAY in config:
        sens = await sensor.new_sensor(config[CONF_PV2_TODAY])
        cg.add(var.set_pv2_today_production_sensor(sens))
                
    if CONF_PV2_TOTAL in config:
        sens = await sensor.new_sensor(config[CONF_PV2_TOTAL])
        cg.add(var.set_pv2_total_production_sensor(sens))
    
    for i, p in enumerate([CONF_PAC_TO_USER, CONF_PAC_TO_LOAD, CONF_PAC_TO_GRID]):
        if p not in config:
            continue

        p_config = config[p]
        for j, sensor_type in enumerate(p_config):
            if sensor_type in p_config:
                sens = await sensor.new_sensor(p_config[sensor_type])
                cg.add(getattr(var, f"set_{p}_sensor")(j, sens))
                
    if CONF_REC_TEMPERATURE in config:
        sens = await sensor.new_sensor(config[CONF_REC_TEMPERATURE])
        cg.add(var.set_rec_temperature_sensor(sens))
    
    if CONF_BATTERY_TEMPERATURE in config:
        sens = await sensor.new_sensor(config[CONF_BATTERY_TEMPERATURE])
        cg.add(var.set_battery_temperature_sensor(sens))
                
                
    if CONF_ENERGY_TOUSER in config:
        p = CONF_ENERGY_TOUSER
        p_config = config[CONF_ENERGY_TOUSER]
        for j, sensor_type in enumerate(p_config):
            if sensor_type in p_config:
                sens = await sensor.new_sensor(p_config[sensor_type])
                cg.add(getattr(var, f"set_{p}_{sensor_type}_sensor")(sens))
                
    if CONF_ENERGY_TOGRID in config:
        p = CONF_ENERGY_TOGRID
        p_config = config[CONF_ENERGY_TOGRID]
        for j, sensor_type in enumerate(p_config):
            if sensor_type in p_config:
                sens = await sensor.new_sensor(p_config[sensor_type])
                cg.add(getattr(var, f"set_{p}_{sensor_type}_sensor")(sens))
                
    if CONF_ENERGY_TOLOAD in config:
        p = CONF_ENERGY_TOLOAD
        p_config = config[CONF_ENERGY_TOLOAD]
        for j, sensor_type in enumerate(p_config):
            if sensor_type in p_config:
                sens = await sensor.new_sensor(p_config[sensor_type])
                cg.add(getattr(var, f"set_{p}_{sensor_type}_sensor")(sens))
                
    if CONF_ENERGY_CHARGE in config:
        p = CONF_ENERGY_CHARGE
        p_config = config[CONF_ENERGY_CHARGE]
        for j, sensor_type in enumerate(p_config):
            if sensor_type in p_config:
                sens = await sensor.new_sensor(p_config[sensor_type])
                cg.add(getattr(var, f"set_{p}_{sensor_type}_sensor")(sens))
                
    if CONF_ENERGY_DISCHARGE in config:
        p = CONF_ENERGY_DISCHARGE
        p_config = config[CONF_ENERGY_DISCHARGE]
        for j, sensor_type in enumerate(p_config):
            if sensor_type in p_config:
                sens = await sensor.new_sensor(p_config[sensor_type])
                cg.add(getattr(var, f"set_{p}_{sensor_type}_sensor")(sens))