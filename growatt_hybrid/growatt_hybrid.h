#pragma once

#include "esphome/core/component.h"
#include "esphome/components/sensor/sensor.h"
#include "esphome/components/modbus/modbus.h"

namespace esphome {
namespace growatt_hybrid {

static const float TWO_DEC_UNIT = 0.01;
static const float ONE_DEC_UNIT = 0.1;

enum GrowattProtocolVersion {
  RTU = 0,
  RTU2,
};

class GrowattHybrid : public PollingComponent, public modbus::ModbusDevice {
 public:
  void update() override;
  void on_modbus_data(const std::vector<uint8_t> &data) override;
  void dump_config() override;
  
  void queryBus();

  void set_protocol_version(GrowattProtocolVersion protocol_version) { this->protocol_version_ = protocol_version; }

  void set_inverter_status_sensor(sensor::Sensor *sensor) { this->inverter_status_ = sensor; }

  void set_grid_frequency_sensor(sensor::Sensor *sensor) { this->grid_frequency_sensor_ = sensor; }
  void set_grid_active_power_sensor(sensor::Sensor *sensor) { this->grid_active_power_sensor_ = sensor; }
  void set_pv_active_power_sensor(sensor::Sensor *sensor) { this->pv_active_power_sensor_ = sensor; }

  void set_today_production_sensor(sensor::Sensor *sensor) { this->today_production_ = sensor; }
  void set_total_energy_production_sensor(sensor::Sensor *sensor) { this->total_energy_production_ = sensor; }
  void set_inverter_module_temp_sensor(sensor::Sensor *sensor) { this->inverter_module_temp_ = sensor; }
  void set_inverter_ipm_temp_sensor(sensor::Sensor *sensor) { this->inverter_ipm_temp_ = sensor; }
  void set_inverter_boost_temp_sensor(sensor::Sensor *sensor) { this->inverter_boost_temp_ = sensor; }
  
  void set_battery_soc_sensor(sensor::Sensor *sensor) { this->battery_soc_ = sensor; }
  void set_battery_vdc_sensor(sensor::Sensor *sensor) { this->battery_vdc_ = sensor; }
  void set_battery_charge_power_sensor(sensor::Sensor *sensor) { this->battery_charge_power_ = sensor; }
  void set_battery_discharge_power_sensor(sensor::Sensor *sensor) { this->battery_discharge_power_ = sensor; }
  void set_battery_temperature_sensor(sensor::Sensor *sensor) { this->battery_temperature_ = sensor; }
  void set_rec_temperature_sensor(sensor::Sensor *sensor) { this->rec_temperature_ = sensor; }
  
  void set_grid_vac_rs_sensor(sensor::Sensor *sensor) { this->grid_vac_rs_ = sensor; }
  void set_grid_vac_st_sensor(sensor::Sensor *sensor) { this->grid_vac_st_ = sensor; }
  void set_grid_vac_tr_sensor(sensor::Sensor *sensor) { this->grid_vac_tr_ = sensor; }
  
  void set_pv1_today_production_sensor(sensor::Sensor *sensor) { this->pv1_today_production_ = sensor; }
  void set_pv1_total_production_sensor(sensor::Sensor *sensor) { this->pv1_total_production_ = sensor; }
  void set_pv2_today_production_sensor(sensor::Sensor *sensor) { this->pv2_today_production_ = sensor; }
  void set_pv2_total_production_sensor(sensor::Sensor *sensor) { this->pv2_total_production_ = sensor; }
  
  void set_eac_today_sensor(sensor::Sensor *sensor) { this->eac_today_ = sensor; }
  void set_eac_total_sensor(sensor::Sensor *sensor) { this->eac_total_ = sensor; }

  //void set_etg_today_sensor(sensor::Sensor *sensor) { this->etg_today_ = sensor; }
  //void set_etg_total_sensor(sensor::Sensor *sensor) { this->etg_total_ = sensor; }
  
  void set_voltage_sensor(uint8_t phase, sensor::Sensor *voltage_sensor) {
    this->phases_[phase].voltage_sensor_ = voltage_sensor;
  }
  void set_current_sensor(uint8_t phase, sensor::Sensor *current_sensor) {
    this->phases_[phase].current_sensor_ = current_sensor;
  }
  void set_active_power_sensor(uint8_t phase, sensor::Sensor *active_power_sensor) {
    this->phases_[phase].active_power_sensor_ = active_power_sensor;
  }
  void set_voltage_sensor_pv(uint8_t pv, sensor::Sensor *voltage_sensor) {
    this->pvs_[pv].voltage_sensor_ = voltage_sensor;
  }
  void set_current_sensor_pv(uint8_t pv, sensor::Sensor *current_sensor) {
    this->pvs_[pv].current_sensor_ = current_sensor;
  }
  void set_active_power_sensor_pv(uint8_t pv, sensor::Sensor *active_power_sensor) {
    this->pvs_[pv].active_power_sensor_ = active_power_sensor;
  }
  
  void set_pac_to_user_sensor(uint8_t phase, sensor::Sensor *sensor) {
    this->pac_to_user_[phase] = sensor;
  }
  void set_pac_to_grid_sensor(uint8_t phase, sensor::Sensor *sensor) {
    this->pac_to_grid_[phase] = sensor;
  }
  void set_pac_to_load_sensor(uint8_t phase, sensor::Sensor *sensor) {
    this->pac_to_local_[phase] = sensor;
  }
  
  void set_energy_to_user_today_sensor(sensor::Sensor *sensor) { this->energy_touser_today_ = sensor; }
  void set_energy_to_user_total_sensor(sensor::Sensor *sensor) { this->energy_touser_total_ = sensor; }
  void set_energy_to_grid_today_sensor(sensor::Sensor *sensor) { this->energy_togrid_today_ = sensor; }
  void set_energy_to_grid_total_sensor(sensor::Sensor *sensor) { this->energy_togrid_total_ = sensor; }
  void set_energy_to_load_today_sensor(sensor::Sensor *sensor) { this->energy_toload_today_ = sensor; }
  void set_energy_to_load_total_sensor(sensor::Sensor *sensor) { this->energy_toload_total_ = sensor; }
  void set_energy_charge_today_sensor(sensor::Sensor *sensor) { this->energy_charge_today_ = sensor; }
  void set_energy_charge_total_sensor(sensor::Sensor *sensor) { this->energy_charge_total_ = sensor; }
  void set_energy_discharge_today_sensor(sensor::Sensor *sensor) { this->energy_discharge_today_ = sensor; }
  void set_energy_discharge_total_sensor(sensor::Sensor *sensor) { this->energy_discharge_total_ = sensor; }

 protected:
  struct GrowattPhase {
    sensor::Sensor *voltage_sensor_{nullptr};
    sensor::Sensor *current_sensor_{nullptr};
    sensor::Sensor *active_power_sensor_{nullptr};
  } phases_[3];
  struct GrowattPV {
    sensor::Sensor *voltage_sensor_{nullptr};
    sensor::Sensor *current_sensor_{nullptr};
    sensor::Sensor *active_power_sensor_{nullptr};
  } pvs_[2];
  
  sensor::Sensor *pac_to_user_[4]{nullptr};
  sensor::Sensor *pac_to_grid_[4]{nullptr};
  sensor::Sensor *pac_to_local_[4]{nullptr};

  sensor::Sensor *inverter_status_{nullptr};

  sensor::Sensor *grid_frequency_sensor_{nullptr};
  sensor::Sensor *grid_active_power_sensor_{nullptr};

  sensor::Sensor *pv_active_power_sensor_{nullptr};

  sensor::Sensor *today_production_{nullptr};
  sensor::Sensor *total_energy_production_{nullptr};
  sensor::Sensor *inverter_module_temp_{nullptr};
  sensor::Sensor *inverter_ipm_temp_{nullptr};
  sensor::Sensor *inverter_boost_temp_{nullptr};
  
  GrowattProtocolVersion protocol_version_;
  
  uint8_t modbus_query_status = 0;
  
  sensor::Sensor *battery_soc_{nullptr};
  sensor::Sensor *battery_vdc_{nullptr};
  sensor::Sensor *battery_charge_power_{nullptr};
  sensor::Sensor *battery_discharge_power_{nullptr};
  
  sensor::Sensor *battery_temperature_{nullptr};
  sensor::Sensor *rec_temperature_{nullptr};
  
  sensor::Sensor *grid_vac_rs_{nullptr};
  sensor::Sensor *grid_vac_st_{nullptr};
  sensor::Sensor *grid_vac_tr_{nullptr};
  
  sensor::Sensor *pv1_today_production_{nullptr};
  sensor::Sensor *pv1_total_production_{nullptr};
  sensor::Sensor *pv2_today_production_{nullptr};
  sensor::Sensor *pv2_total_production_{nullptr};
  
  sensor::Sensor *eac_today_{nullptr};
  sensor::Sensor *eac_total_{nullptr};
  //sensor::Sensor *etg_today_{nullptr};
  //sensor::Sensor *etg_total_{nullptr};
  
  sensor::Sensor *energy_touser_today_{nullptr};
  sensor::Sensor *energy_touser_total_{nullptr};
  sensor::Sensor *energy_togrid_today_{nullptr};
  sensor::Sensor *energy_togrid_total_{nullptr};
  sensor::Sensor *energy_toload_today_{nullptr};
  sensor::Sensor *energy_toload_total_{nullptr};
  sensor::Sensor *energy_charge_today_{nullptr};
  sensor::Sensor *energy_charge_total_{nullptr};
  sensor::Sensor *energy_discharge_today_{nullptr};
  sensor::Sensor *energy_discharge_total_{nullptr};
  
};

}  // namespace growatt_solar
}  // namespace esphome
