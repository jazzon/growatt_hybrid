#include "growatt_hybrid.h"
#include "esphome/core/log.h"

namespace esphome {
namespace growatt_hybrid {

static const char *const TAG = "growatt_hybrid";

static const uint8_t MODBUS_CMD_READ_IN_REGISTERS = 0x04;
static const uint8_t MODBUS_REGISTER_COUNT[] = {33, 96};  // indexed with enum GrowattProtocolVersion

void GrowattHybrid::update() {
  this->modbus_query_status = 0;
  this->queryBus();
}

void GrowattHybrid::queryBus() {
  if(this->modbus_query_status == 0)
    this->send(MODBUS_CMD_READ_IN_REGISTERS, 0, MODBUS_REGISTER_COUNT[this->protocol_version_]);
  else if(this->modbus_query_status == 1)
    this->send(MODBUS_CMD_READ_IN_REGISTERS, 1000, 65); // (125 for battery BMS status registers)
  
  // this->modbus_query_status++;
}

void GrowattHybrid::on_modbus_data(const std::vector<uint8_t> &data) {
  auto publish_1_reg_sensor_state = [&](sensor::Sensor *sensor, size_t i, float unit) -> void {
    if (sensor == nullptr)
      return;
    float value = encode_uint16(data[i * 2], data[i * 2 + 1]) * unit;
    sensor->publish_state(value);
  };

  auto publish_2_reg_sensor_state = [&](sensor::Sensor *sensor, size_t reg1, size_t reg2, float unit) -> void {
    float value = ((encode_uint16(data[reg1 * 2], data[reg1 * 2 + 1]) << 16) +
                   encode_uint16(data[reg2 * 2], data[reg2 * 2 + 1])) *
                  unit;
    if (sensor != nullptr)
      sensor->publish_state(value);
  };

  switch (this->protocol_version_) {
    case RTU: {
      publish_1_reg_sensor_state(this->inverter_status_, 0, 1);

      publish_2_reg_sensor_state(this->pv_active_power_sensor_, 1, 2, ONE_DEC_UNIT);

      publish_1_reg_sensor_state(this->pvs_[0].voltage_sensor_, 3, ONE_DEC_UNIT);
      publish_1_reg_sensor_state(this->pvs_[0].current_sensor_, 4, ONE_DEC_UNIT);
      publish_2_reg_sensor_state(this->pvs_[0].active_power_sensor_, 5, 6, ONE_DEC_UNIT);

      publish_1_reg_sensor_state(this->pvs_[1].voltage_sensor_, 7, ONE_DEC_UNIT);
      publish_1_reg_sensor_state(this->pvs_[1].current_sensor_, 8, ONE_DEC_UNIT);
      publish_2_reg_sensor_state(this->pvs_[1].active_power_sensor_, 9, 10, ONE_DEC_UNIT);

      publish_2_reg_sensor_state(this->grid_active_power_sensor_, 11, 12, ONE_DEC_UNIT);
      publish_1_reg_sensor_state(this->grid_frequency_sensor_, 13, TWO_DEC_UNIT);

      publish_1_reg_sensor_state(this->phases_[0].voltage_sensor_, 14, ONE_DEC_UNIT);
      publish_1_reg_sensor_state(this->phases_[0].current_sensor_, 15, ONE_DEC_UNIT);
      publish_2_reg_sensor_state(this->phases_[0].active_power_sensor_, 16, 17, ONE_DEC_UNIT);

      publish_1_reg_sensor_state(this->phases_[1].voltage_sensor_, 18, ONE_DEC_UNIT);
      publish_1_reg_sensor_state(this->phases_[1].current_sensor_, 19, ONE_DEC_UNIT);
      publish_2_reg_sensor_state(this->phases_[1].active_power_sensor_, 20, 21, ONE_DEC_UNIT);

      publish_1_reg_sensor_state(this->phases_[2].voltage_sensor_, 22, ONE_DEC_UNIT);
      publish_1_reg_sensor_state(this->phases_[2].current_sensor_, 23, ONE_DEC_UNIT);
      publish_2_reg_sensor_state(this->phases_[2].active_power_sensor_, 24, 25, ONE_DEC_UNIT);

      publish_2_reg_sensor_state(this->today_production_, 26, 27, ONE_DEC_UNIT);
      publish_2_reg_sensor_state(this->total_energy_production_, 28, 29, ONE_DEC_UNIT);

      publish_1_reg_sensor_state(this->inverter_module_temp_, 32, ONE_DEC_UNIT);
      break;
    }
    case RTU2: {
      if(this->modbus_query_status == 0) {
        publish_1_reg_sensor_state(this->inverter_status_, 0, 1);

        publish_2_reg_sensor_state(this->pv_active_power_sensor_, 1, 2, ONE_DEC_UNIT);

        publish_1_reg_sensor_state(this->pvs_[0].voltage_sensor_, 3, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->pvs_[0].current_sensor_, 4, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pvs_[0].active_power_sensor_, 5, 6, ONE_DEC_UNIT);

        publish_1_reg_sensor_state(this->pvs_[1].voltage_sensor_, 7, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->pvs_[1].current_sensor_, 8, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pvs_[1].active_power_sensor_, 9, 10, ONE_DEC_UNIT);

        publish_2_reg_sensor_state(this->grid_active_power_sensor_, 35, 36, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->grid_frequency_sensor_, 37, TWO_DEC_UNIT);

        publish_1_reg_sensor_state(this->phases_[0].voltage_sensor_, 38, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->phases_[0].current_sensor_, 39, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->phases_[0].active_power_sensor_, 40, 41, ONE_DEC_UNIT);

        publish_1_reg_sensor_state(this->phases_[1].voltage_sensor_, 42, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->phases_[1].current_sensor_, 43, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->phases_[1].active_power_sensor_, 44, 45, ONE_DEC_UNIT);

        publish_1_reg_sensor_state(this->phases_[2].voltage_sensor_, 46, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->phases_[2].current_sensor_, 47, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->phases_[2].active_power_sensor_, 48, 49, ONE_DEC_UNIT);

        publish_2_reg_sensor_state(this->today_production_, 53, 54, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->total_energy_production_, 91, 92, ONE_DEC_UNIT);
        
        publish_2_reg_sensor_state(this->eac_today_, 53, 54, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->eac_total_, 55, 56, ONE_DEC_UNIT);

        publish_2_reg_sensor_state(this->pv1_today_production_, 59, 60, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pv1_total_production_, 61, 62, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pv2_today_production_, 63, 64, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pv2_total_production_, 65, 66, ONE_DEC_UNIT);

        publish_1_reg_sensor_state(this->inverter_module_temp_, 93, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->inverter_ipm_temp_, 94, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->inverter_boost_temp_, 95, ONE_DEC_UNIT);
        
        publish_1_reg_sensor_state(this->grid_vac_rs_, 50, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->grid_vac_st_, 51, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->grid_vac_tr_, 52, ONE_DEC_UNIT);
        
        this->modbus_query_status++;
        
      } else if(this->modbus_query_status == 1) {
        publish_1_reg_sensor_state(this->battery_vdc_, 13, ONE_DEC_UNIT); // battery voltage at 1013
        publish_1_reg_sensor_state(this->battery_soc_, 14, 1); // battery SOC at 1014
        
        publish_2_reg_sensor_state(this->battery_charge_power_, 11, 12, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->battery_discharge_power_, 9, 10, ONE_DEC_UNIT);
        
        publish_2_reg_sensor_state(this->pac_to_user_[0], 15, 16, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_user_[1], 17, 18, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_user_[2], 19, 20, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_user_[3], 21, 22, ONE_DEC_UNIT);
        
        publish_2_reg_sensor_state(this->pac_to_grid_[0], 23, 24, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_grid_[1], 25, 26, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_grid_[2], 27, 28, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_grid_[3], 29, 30, ONE_DEC_UNIT);
        
        publish_2_reg_sensor_state(this->pac_to_local_[0], 31, 32, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_local_[1], 33, 34, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_local_[2], 35, 36, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->pac_to_local_[3], 37, 38, ONE_DEC_UNIT);
        
        publish_1_reg_sensor_state(this->battery_temperature_, 40, ONE_DEC_UNIT);
        publish_1_reg_sensor_state(this->rec_temperature_, 39, ONE_DEC_UNIT);
        
        publish_2_reg_sensor_state(this->energy_touser_today_, 44, 45, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_touser_total_, 46, 47, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_togrid_today_, 48, 49, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_togrid_total_, 50, 51, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_toload_today_, 60, 61, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_toload_total_, 62, 63, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_charge_today_, 56, 57, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_charge_total_, 58, 59, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_discharge_today_, 52, 53, ONE_DEC_UNIT);
        publish_2_reg_sensor_state(this->energy_discharge_total_, 54, 55, ONE_DEC_UNIT);
        
        this->modbus_query_status++;
      }
      
      this->queryBus();
      
      break;
    }
  }
}

void GrowattHybrid::dump_config() {
  ESP_LOGCONFIG(TAG, "GROWATT Hybrid:");
  ESP_LOGCONFIG(TAG, "  Address: 0x%02X", this->address_);
}

}  // namespace growatt_solar
}  // namespace esphome
